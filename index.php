<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <title>Hola, Mundo cruel!</title>

  </head>
  <body>
  <div class="container">
  <h1 style="margin-top:100px;">Hola!! soy cortana y ... naaa </h1>
    <p>esta es una prueba de ajax que llama a un archivo php el cual tiene un sleep de 10 segundos antes de responder.</p>
    <p>Esta prueba la hice porque me estaba fallando el ajax en un proyecto YII 1 que al pasar al servidor los archivos el ajax cargaba todo y no esperaba sus pasos</p>
    <p>mira la consola y presiona el botón</p>
    <button type="button" onclick="enviar();">enviar</button>
    <span id="respuesta">
    </span>
    <!--begin::Modals -->
    <div class="modal fade" id="before" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Esta wea es un loading...</h5>
            </div>
            <div class="modal-body">
                cargando... <br><i>tiene una espera de 10 segundos en el servidor</i>
            </div>
            <div class="modal-footer">
            </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Y esta wea es la respuesta....</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Funciona! el servidor responde: <span id=txtnombre></span>. <br>
                <p>si funciona esta wea porque mierda no me funciona en el servidor con YII? xD!</p>
            </div>
            <div class="modal-footer">
            </div>
            </div>
        </div>
    </div>
    <!--end::Modals -->
  </div>
  
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script type="text/javascript">
    function enviar(){
        console.log('antes de ajax');
        $.ajax({
            url:   '/testing-ajax/data.php',
            type:  'post',
            beforeSend: function () {
                console.log('before');
                $('#before').modal('show');
            },
            success:  function (data) {
                console.log('success');
                $('#before').modal('hide');
                $('#txtnombre').html(data);
                $('#success').modal('show');
            },
            complete: function(){
                console.log('complete');
            },
            error: function(){
                console.log('error');
            }
        });
    }
    </script>
  </body>
</html>